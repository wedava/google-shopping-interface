from django.contrib import admin
from .models import CredentialsModel, FlowModel


admin.site.register(CredentialsModel)
admin.site.register(FlowModel)
