from django.conf.urls import url, patterns
from .views import *

urlpatterns = patterns('',
                       url(r'^$', index, name='index'),
                       url(r'^oauth2callback', auth_return, name='return'),
                       url(r'^add_product/$', add_product, name='add'),
                       )
