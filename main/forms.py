from django import forms

AVAILABILITY_CHOICES = (
    ('in stock', 'in stock'),
    ('out of stock', 'out of stock'),
    ('preorder', 'preorder'),
)

CONDITION_CHOICES = (
    ('new', 'new'),
    ('refurbished', 'refurbished'),
    ('used', 'used'),
)

TAX_ON_SHIPPING_CHOICES = (
    ('True', 'Yes'),
    ('False', 'No'),

)

class UpdateForm(forms.Form):
    product_id = forms.CharField()
    price = forms.FloatField(required=False)
    currency = forms.CharField(max_length=20, initial="USD")
    availability = forms.ChoiceField(choices=AVAILABILITY_CHOICES)
    quantity = forms.IntegerField(required=False)


class InsertForm(forms.Form):
    offerId = forms.CharField(help_text="required")
    title = forms.CharField()
    description = forms.CharField(widget=forms.Textarea)
    productType = forms.CharField()
    condition = forms.ChoiceField(choices=CONDITION_CHOICES)
    availability = forms.ChoiceField(choices=AVAILABILITY_CHOICES)
    brand = forms.CharField()
    gtin = forms.CharField()
    mpn = forms.CharField()
    price = forms.FloatField()
    currency = forms.CharField(max_length=5, initial="USD")
    link = forms.CharField()
    imageLink = forms.CharField()
    shippingPrice = forms.FloatField()
    shippingCurrency = forms.CharField(max_length=5, initial="USD")
    taxRate = forms.FloatField()
    taxLocation = forms.IntegerField()
    taxRegion = forms.CharField(max_length=5)
    taxShipping = forms.ChoiceField(choices=TAX_ON_SHIPPING_CHOICES)

