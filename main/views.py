from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.decorators import login_required
from oauth2client.client import flow_from_clientsecrets
from django.template.context import RequestContext
from .models import CredentialsModel, FlowModel
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from oauth2client.django_orm import Storage
from googleapiclient.errors import HttpError
from django.core.urlresolvers import reverse
from googleapiclient.discovery import build
from .forms import UpdateForm, InsertForm
from django.contrib import messages
from oauth2client import xsrfutil
from django.conf import settings
import oauth2client
import httplib2
import os
from django.contrib.auth.views import login as contrib_login

def login(request):
    if request.user.is_authenticated():
        return redirect(settings.LOGIN_REDIRECT_URL)
    return contrib_login(request)

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secret.json')


@login_required
def index(request):
    form = UpdateForm()
    user = request.user
    storage = Storage(CredentialsModel, 'id', user, 'credential')
    credential = storage.get()
    if not settings.DEBUG:
        FLOW = flow_from_clientsecrets(
            CLIENT_SECRETS,
            scope='https://www.googleapis.com/auth/content',
            redirect_uri="https://%s%s" % (get_current_site(request).domain, reverse("oauth2:return")))
    else:
        FLOW = flow_from_clientsecrets(
            CLIENT_SECRETS,
            scope='https://www.googleapis.com/auth/content',
            redirect_uri='https://127.0.0.1:8000/oauth2callback')
    if credential is None or credential == '' or not credential:
        FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY, user)
        authorize_url = FLOW.step1_get_authorize_url()
        f = FlowModel(id=user, flow=FLOW)
        f.save()
        return HttpResponseRedirect(authorize_url)
    else:
        http = httplib2.Http()
        http = credential.authorize(http)
        service = build('content', 'v2', http=http)
        try:
            ids = service.accounts().authinfo().execute()
            merchant_id = ids['accountIdentifiers'][0]['merchantId']
            request.session['merchant'] = merchant_id
            pds = service.products().list(merchantId=merchant_id).execute()
            try:
                products_list = pds["resources"]
            except KeyError:
                products_list = []
            if request.POST:
                form = UpdateForm(request.POST)
                if form.is_valid():
                    data = form.cleaned_data
                    body = {}
                    if data['price'] != '' and data['currency'] != '':
                        body['price'] = {"currency": data['currency'], "value": str(data['price'])}
                    if data['quantity'] != '':
                        body['quantity'] = data['quantity']
                    if data['availability'] != '':
                        body['availability'] = data['availability']
                    try:
                        update = service.inventory().set(merchantId=merchant_id, storeCode='online',
                                                         productId=str(data['product_id']), body=body).execute()
                    except oauth2client.client.AccessTokenRefreshError:
                        messages.error(request, "Please ensure the data entered is valid")
                        return HttpResponseRedirect(reverse("oauth2:index"))
                    messages.success(request, "Product updated")
                    return HttpResponseRedirect(reverse("oauth2:index"))

        except oauth2client.client.AccessTokenRefreshError:
            FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY, user)
            authorize_url = FLOW.step1_get_authorize_url()
            return HttpResponseRedirect(authorize_url)
        return render_to_response('index.html',
                                  {'product_list': products_list, 'merchant_id': merchant_id, 'form': form},
                                  context_instance=RequestContext(request))


@login_required
def auth_return(request):
    user = request.user
    if not settings.DEBUG:
        FLOW = flow_from_clientsecrets(
            CLIENT_SECRETS,
            scope='https://www.googleapis.com/auth/content',
            redirect_uri="https://%s%s" % (get_current_site(request).domain, reverse("oauth2:return")))
    else:
        FLOW = flow_from_clientsecrets(
            CLIENT_SECRETS,
            scope='https://www.googleapis.com/auth/content',
            redirect_uri='https://127.0.0.1:8000/oauth2callback')
    if not xsrfutil.validate_token(settings.SECRET_KEY, bytes(request.GET['state'], 'utf-8'), user):
        return HttpResponseBadRequest()
    credential = FLOW.step2_exchange(request.GET, http=None)
    storage = Storage(CredentialsModel, 'id', user, 'credential')
    storage.put(credential)
    return HttpResponseRedirect(reverse("oauth2:index"))


@login_required
def add_product(request):
    form = InsertForm()
    user = request.user
    storage = Storage(CredentialsModel, 'id', user, 'credential')
    credential = storage.get()
    if not settings.DEBUG:
        FLOW = flow_from_clientsecrets(
            CLIENT_SECRETS,
            scope='https://www.googleapis.com/auth/content',
            redirect_uri="https://%s%s" % (get_current_site(request).domain, reverse("oauth2:return")))
    else:
        FLOW = flow_from_clientsecrets(
            CLIENT_SECRETS,
            scope='https://www.googleapis.com/auth/content',
            redirect_uri='https://127.0.0.1:8000/oauth2callback')
    if credential is None or credential == '':
        FLOW.params['state'] = xsrfutil.generate_token(
            settings.SECRET_KEY, user)
        authorize_url = FLOW.step1_get_authorize_url()
        f = FlowModel(id=user, flow=FLOW)
        f.save()
        return HttpResponseRedirect(authorize_url)
    else:
        http = httplib2.Http()
        http = credential.authorize(http)
        service = build('content', 'v2', http=http)
        merchant_id = request.session['merchant']
        try:
            if request.POST:
                form = InsertForm(request.POST)
                if form.is_valid():
                    data = form.cleaned_data
                    body = {'channel': 'online', 'contentLanguage': 'EN', 'targetCountry': 'US',
                            'googleProductCategory': 'Vehicles & Parts > Vehicle Parts & Accessories'}
                    if data['price'] != '' and data['currency'] != '':
                        body['price'] = {"currency": data['currency'], "value": str(data['price'])}
                    if data['availability'] != '':
                        body['availability'] = data['availability']
                    if data['brand'] != '':
                        body['brand'] = data['brand']
                    if data['condition'] != '':
                        body['condition'] = data['condition']
                    if data['description'] != '':
                        body['description'] = data['description']
                    if data['gtin'] != '':
                        body['gtin'] = data['gtin']
                    if data['imageLink'] != '':
                        body['imageLink'] = data['imageLink']
                    if data['productType'] != '':
                        body['productType'] = data['productType']
                    if data['shippingPrice'] != '' and data['shippingPrice'] != '':
                        body['shipping'] = [
                            {'price': {'value': data['shippingPrice'], 'currency': data['shippingCurrency']}}]
                    if data['taxRate'] != '' and data['taxLocation'] != '':
                        body['taxes'] = [
                            {'rate': data['taxRate'], 'country': 'US',
                             'region': data['taxRegion'], 'taxShip': data['taxShipping'], }]
                    if data['link'] != '':
                        body['link'] = data['link']
                    if data['mpn'] != '':
                        body['mpn'] = data['mpn']
                    if data['title'] != '':
                        body['title'] = data['title']
                    if data['offerId'] != '':
                        body['offerId'] = data['offerId']
                    try:
                        insert = service.products().insert(merchantId=merchant_id, body=body)
                        result = insert.execute()
                    except HttpError:
                        messages.error(request, "Please ensure the data entered is valid")
                        return HttpResponseRedirect('/add_product/')
                    messages.success(request, "Product inserted")
                    return HttpResponseRedirect('/')
        except oauth2client.client.AccessTokenRefreshError:
            FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY, user)
            authorize_url = FLOW.step1_get_authorize_url()
            return HttpResponseRedirect(authorize_url)
    return render_to_response('add_product.html', locals(), context_instance=RequestContext(request))
