### Interface for Google-Shopping API

#### Platform requirements
<p>Python 3.4</p>
<p>Django 1.8</p>

#### Setup instructions
<p>First and foremost, the necessarry settings(e.g redirect URIs) should be configured in the Google developer console
 for the application. The credentials json file should then be downloaded, renamed to client_secret.json and copied into
 the <code>main</code> module.</p>

<p>After the above is done, the following steps should be taken to setup and access the application</p>

<ul>
    <li>Install virtualenv, create a virtual envirnment for the application and install the requirements.
    <p>Make sure you use python 3.4 as the interpreter when creating the virtualenv. The requirements are listed in the
    requirements.txt file and can be installed using <code>pip install -r requirements.txt<code></li>
    <li>Create the database tables and create a superuser</li>
     <p>Run the migrations in order to create the tables. This can be done by running the command
     <code>./manage.py migrate</code></p>
     <p>After running the migrations run <code>./manage.py syncdb</code>. When asked to create a superuser, proceed
     with yes and create your superuser. The superuser's credentials will be required to log into the interface.
    <li>Run the development server on https</li>
     <p><code>python manage.py runsslserver [--addrport 127.0.0.1:9000]</code>
     <p>The development server should be run on https to avoid conflicts with google oauth2 which redirects to https</p>
     <p>This also means that for production the application should also be configured to allow traffic on https</p>
    <li>Access the application on localhost port 8000 through https (https://localhost:8000/)</li>
    <p>You will probably get a page warning about the security certificate. Ignore the warning and proceed.</p>
</ul>

<p>When you grant the application permissions to edit your merchant center, you will be redirected to a page with a
list of your merchant center products and a form that can be used to update the product details. The details that
can be updated are <strong>price</strong>, <strong>currency</strong>, <strong>availability</strong> and <strong>quantity</strong>.
You can update the price only or the quantity only or both.

<p>There is also a page for adding products. Here, all details are required. Various placeholder text are available to direct
the user on values expected for the fields.

#### Additional notes

<p>The application comes bundled with an edited copy of googleapiclient. This is because the source contains various
pieces of code that prevent proper function when using Python 3. Example of such a case is the [following issue on gihtub](https://github.com/google/oauth2client/issues/168)